// Voeg iets aan lijst toe, en zorg ervoor dat alles in de lijst weergegeven wordt.
// Daarna; Zorg ervoor dat alles in de lijst automatisch weergegeven wordt als je lijst aanpast, zonder dat je dan ook de loop hoeft te wijzigen.

let lijst =
    [
        "Appel", "Peer", "Banaan"
    ];

console.log("Loopen door lijst van lengte:", lijst.length);

for (let i = 0; i < 3; i++)
{
    console.log("lijst[", i, "]: ", lijst[i]);
}