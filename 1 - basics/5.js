// Maak een Kat instantie (zie nieuwDing voor een hint), zorg ervoor dat de info klopt en laat de neuwe kat miauwen.

class Ding
{
    nummer = 0;

    constructor (getal)
    {
        this.nummer = getal;
    }
}

class Kat
{
    naam = "niks";
    leeftijd = -1;

    constructor (naam)
    {
        this.naam = naam;
    }

    miauw ()
    {
        for (let i = 0; i < this.leeftijd; i++)
            console.log(this.naam + ":", "Miauw");
    }
}

let nieuwDing = new Ding(5);
console.log(nieuwDing.nummer);