// Verander vermenigvuldig(getal) zodat ik een tweede getal kan opgeven waar het eerste getal mee vermenigvuldigd wordt.

function vermenigvuldig (getal)
{
    let resultaat = getal * 2;
    return resultaat;
}

let vermenigvuldigs = vermenigvuldig(15);