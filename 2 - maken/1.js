//Maak een object met een variabele en twee functies
// Variabele: register - een variabele om een waarde in te bewaren
// Functie 1: Save(variable) - pak de inhoud van `variable` en sla deze op in de `register` van het object
// Functie 2: Load() - Pak de inhoud van `register`, en return die

// Begin

class Storage
{
    register = null;

    Save (variable)
    {

    }

    Load ()
    {

    }
}

// Einde

let storer = new Storage();

console.log("Saving:", "kip");
storer.Save("kip");
console.log("Loaded: ", storer.Load());

console.log("Saving", 5);
storer.Save(5);
console.log("Loaded: ", storer.Load());