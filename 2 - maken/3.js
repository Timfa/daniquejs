/**
 * Taak: Vul de Messagecontainer class in met variables en functies om de volgende taken uit te kunnen voeren:
 * - Een bericht kan aangemaakt worden met de naam van een auteur en een bericht
 * - Een bericht kan bewerkt worden, en de hoeveelheid edits wordt bijgehouden
 * 
 *  Enkel de class definitie mag aangepast worden!
 *  Onder de class staat wat code; als deze succesvol uitgevoerd wordt, is het gelukt!
 */

class MessageContainer
{

}

///////Einde

let msg = new MessageContainer("Tim", "Oorspronkelijk bericht");

console.log("Nieuw bericht gemaakt. Verwachte waardes: 'Tim', 'Oorspronkelijk bericht', '0': ", msg);

msg.edit("bericht aangepast");

console.log("bericht eenmaal bewerkt. Nieuwe verwachtte waardes: 'Tim', 'bericht aangepast', '1': ", msg);

msg.edit("nogmaals aangepast");

console.log("bericht tweemaal bewerkt. Nieuwe verwachtte waardes: 'Tim', 'nogmaals aangepast', '2': ", msg);

msg.edit("nogmaals aangepast");

console.log("BONUS: Bericht edit functie aangeroepen, maar dezelfde tekst aangeleverd. Verwachtte waardes: 'Tim', 'nogmaals aangepast', '2': ", msg);